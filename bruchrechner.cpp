// bruchrechner.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

// uebung7.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//
#include "stdafx.h"
#include <stdio.h>

typedef struct {
	int zaehler;
	int nenner;
}bruch;

void nenner(int *z1, int *n1, int *z2, int *n2) { // Funktion um Brueche auf gleichen Nenner zu bringen
	int zaehler1 = (*z1 * *n2);
	int zaehler2 = (*z2 * *n1);
	int nenner = *n1 * *n2;
	*z1 = zaehler1;
	*z2 = zaehler2;
	*n1 = nenner;
	*n2 = nenner;
}

void kehrwert(int z, int n, int *z2, int *n2) {
	*z2 = n;
	*n2 = z;
}

void kuerzen(int z, int n, int *z2, int *n2) {
	int c;
	int a = z;
	int b = n;


	while (b != 0) {
		c = a%b;
		a = b;
		b = c;
	}
	*z2 = z / a;
	*n2 = n / a;
}

void brueche(char mode, int z1, int n1, int z2, int n2, int *zerg, int *nerg) {

	if (mode == '+') {
		nenner(&z1, &n1, &z2, &n2);
		*zerg = z1 + z2;
		*nerg = n1;
	}
	else if (mode == '-') {
		nenner(&z1, &n1, &z2, &n2);
		*zerg = z1 - z2;
		*nerg = n1;
	}
	else if (mode == '*') {
		*zerg = z1 * z2;
		*nerg = n1 * n2;
	}
	else if (mode == ':') {
		int zkehr;
		int nkehr;
		kehrwert(z2, n2, &zkehr, &nkehr);
		*zerg = z1 * zkehr;
		*nerg = n1 * nkehr;
	}
}

char input() {
	bruch b1, b2, berg, bkurz;

	char operation;
	char temp = 'h'; // dient zum auffangen der Entertaste


	printf("Bitte geben sie den ersten Bruch ein(z. B. 1/2): ");
	scanf_s("%d/%d%c", &b1.zaehler, &b1.nenner, &temp);
	printf("\nWaehlen sie nun die rechenoperation aus + - * oder : ");
	scanf_s("%c%c", &operation, &temp);
	printf("\nGeben sie nun den zweiten Bruch ein: ");
	scanf_s("%d/%d%c", &b2.zaehler, &b2.nenner, &temp);

	brueche(operation, b1.zaehler, b1.nenner, b2.zaehler, b2.nenner, &berg.zaehler, &berg.nenner);
	kuerzen(berg.zaehler, berg.nenner, &bkurz.zaehler, &bkurz.nenner);
	printf("\n%d/%d %c %d/%d = %d/%d = %d/%d", b1.zaehler, b1.nenner, operation, b2.zaehler, b2.nenner, berg.zaehler, berg.nenner, bkurz.zaehler, bkurz.nenner);
	int a;
	int b;
	kehrwert(bkurz.zaehler, bkurz.nenner, &a, &b);
	printf("\nDer kehwert des Ergebnisses ist %d/%d\n\n", a, b);
	char ausgabe;

	printf("Moechten sie nochmehr Brueche berechnen? j fuer Ja und beliebige taste fuer Nein ");
	scanf_s("%c%c", &ausgabe, &temp);
	return ausgabe;
}

int main()
{
	char check;
rechnen: {
	check = input();
}
		 if (check == 'j') {
			 goto rechnen;
		 }
		 return 0;
}
